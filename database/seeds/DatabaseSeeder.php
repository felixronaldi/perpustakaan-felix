<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
	  App\User::create([
        'name' => 'adminfelix',
        'email' => 'adminfelix@email.com',
        'password' => bcrypt('felixfelix'),
        'alamat' => 'dont have',
        'no_handphone' => 'dont have',
        'nim' => 1503123123
      ]);
    }
}
